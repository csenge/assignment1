package edu.ut.dsLab.flightDataAccess.dao;

import edu.ut.dsLab.flightDataAccess.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by cvarga on 10/18/2016.
 */
public interface CityDAO extends JpaRepository<City, String> {
}
