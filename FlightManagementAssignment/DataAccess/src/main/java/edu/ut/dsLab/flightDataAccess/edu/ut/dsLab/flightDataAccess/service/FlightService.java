package edu.ut.dsLab.flightDataAccess.edu.ut.dsLab.flightDataAccess.service;

import edu.ut.dsLab.flightDataAccess.dao.FlightDAO;
import edu.ut.dsLab.flightDataAccess.entity.City;
import edu.ut.dsLab.flightDataAccess.entity.Flight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by cvarga on 10/18/2016.
 */
@Service
public class FlightService {

    @Autowired
    private FlightDAO flightDAO;

    public void insertFlight(Flight flight) {
        flightDAO.save(flight);
    }

    public List<Flight> findFlight(City departureCity, City arrivalCity, Date departureDate, Date arrivalDate) {
        List<Flight> flights = new ArrayList();
        flights = flightDAO.findAll();

        List<Flight> result = new ArrayList<>();

        if (departureCity != null) {
            for (Flight f : flights) {
                if (f.getDepartureCity().equals(departureCity)) {
                    result.add(f);
                }
            }
        }

        if (arrivalCity != null) {
            for (Flight f : flights) {
                if (f.getArrivalCity().equals(arrivalCity)) {
                    result.add(f);
                }
            }
        }

        if (departureDate != null) {
            for (Flight f : flights) {
                if (f.getDepartureDate().equals(departureDate)) {
                    result.add(f);
                }
            }
        }

        if (arrivalDate != null) {
            for (Flight f : flights) {
                if (f.getArrivalDate().equals(arrivalDate)) {
                    result.add(f);
                }
            }
        }

        return flights;

    }

    public void deleteFlight(String flightNumber){
        flightDAO.delete(flightNumber);
    }
}
