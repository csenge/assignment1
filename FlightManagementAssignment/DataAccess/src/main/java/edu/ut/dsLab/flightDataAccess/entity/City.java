package edu.ut.dsLab.flightDataAccess.entity;

/**
 * Created by cvarga on 10/18/2016.
 */
public class City {
    private float latitude;
    private float longitude;

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }
}
