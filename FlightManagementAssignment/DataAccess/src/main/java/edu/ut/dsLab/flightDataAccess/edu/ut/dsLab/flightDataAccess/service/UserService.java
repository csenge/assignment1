package edu.ut.dsLab.flightDataAccess.edu.ut.dsLab.flightDataAccess.service;

import edu.ut.dsLab.flightDataAccess.dao.UserDAO;
import edu.ut.dsLab.flightDataAccess.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cvarga on 10/18/2016.
 */
@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public void insertUser(User user){
        userDAO.save(user);
    }

    public User findUser(String username){
        return userDAO.findOne(username);
    }

    public User updateUserPassword(String username, String newPassword){
        User user=userDAO.findOne(username);
        user.setPassword(newPassword);
        userDAO.save(user);
        return user;
    }

    public void deleteUser(String username){
        userDAO.delete(username);
    }
}
