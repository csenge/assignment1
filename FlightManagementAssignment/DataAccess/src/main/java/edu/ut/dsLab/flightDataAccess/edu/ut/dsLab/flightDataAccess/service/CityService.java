package edu.ut.dsLab.flightDataAccess.edu.ut.dsLab.flightDataAccess.service;

import edu.ut.dsLab.flightDataAccess.dao.CityDAO;
import edu.ut.dsLab.flightDataAccess.entity.City;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by cvarga on 10/18/2016.
 */
@Service
public class CityService {

    @Autowired
    private CityDAO cityDAO;

    public void insertCity(City city){
        cityDAO.save(city);
    }

    public City findCity(String cityName){
        return cityDAO.findOne(cityName);
    }

    public void deleteCity(String cityName){
        cityDAO.delete(cityName);
    }
}
