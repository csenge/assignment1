package edu.ut.dsLab.UIController;


import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by cvarga on 10/27/2016.
 */
@Component
@RequestMapping("/login")
public class LoginController extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response){

        //login
        String username=request.getParameter("login");
        System.out.println(username+" is the username provided");
    }

}
