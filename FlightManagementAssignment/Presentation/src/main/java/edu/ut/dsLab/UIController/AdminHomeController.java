package edu.ut.dsLab.UIController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by cvarga on 10/25/2016.
 */
@Controller
@RequestMapping("/admin")
public class AdminHomeController {

/**
 *  @Autowired
private UserRepository userRepository;

 @RequestMapping(value="", method= RequestMethod.GET)
 public String listPost(Model model) {
 model.addAttribute("users", userRepository.findAll());

 return "admin/list";
 }

 @RequestMapping(value="/{id}/delete", method = RequestMethod.GET)
 public ModelAndView delete(@PathVariable long id) {
 userRepository.delete(id);
 return new ModelAndView("redirect:/admin");
 }

 @RequestMapping(value="new", method = RequestMethod.GET)
 public String newProject() {
 return "admin/new";
 }

 @RequestMapping(value="/create", method = RequestMethod.POST)
 public String create( @RequestParam("user_username") String username,
 @RequestParam("user_password") String password) {
 userRepository.save(new User(username, password, "USER"));
 return new String("redirect:/admin");
 }

 @RequestMapping(value = "/update", method = RequestMethod.POST)
 public ModelAndView update(@RequestParam("user_id") long id,
 @RequestParam("user_username") String username,
 @RequestParam("user_password") String password) {
 User user = userRepository.findOne(id);
 user.setUsername(username);
 user.setPassword(password);
 user.setRole("USER");
 userRepository.save(user);
 return new ModelAndView("redirect:/admin");
 }


 @RequestMapping(value = "/{id}/edit", method = RequestMethod.GET)
 public String edit(@PathVariable long id,
 Model model) {
 User user = userRepository.findOne(id);
 model.addAttribute("user", user);
 return  "admin/edit";
 }
 */
}
